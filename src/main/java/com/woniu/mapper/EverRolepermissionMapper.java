package com.woniu.mapper;

import com.woniu.entity.EverRolepermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
public interface EverRolepermissionMapper extends BaseMapper<EverRolepermission> {

}
