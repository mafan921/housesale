package com.woniu.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverOrder;
import com.woniu.entity.EverSalerecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface EverSalerecordMapper extends BaseMapper<EverSalerecord> {
    //查询每一天的销售流水
    List<EverSalerecord> selectSalary();

    IPage<EverSalerecord> find2UH(Page<EverSalerecord> page, @Param(Constants.WRAPPER) Wrapper<EverSalerecord> wrapper);

    IPage<EverOrder> selectSLByManager(Page<EverOrder> page,@Param(Constants.WRAPPER)QueryWrapper<EverOrder> queryWrapper);

//    List<EverSalerecord> selectList(QueryWrapper<EverOrder> queryWrapper);

}
