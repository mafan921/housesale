package com.woniu.mapper;

import com.woniu.entity.EverEmprole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-12-04
 */
public interface EverEmproleMapper extends BaseMapper<EverEmprole> {

}
