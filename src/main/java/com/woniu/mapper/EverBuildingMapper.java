package com.woniu.mapper;

import com.woniu.entity.EverBuilding;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface EverBuildingMapper extends BaseMapper<EverBuilding> {

}
