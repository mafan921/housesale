package com.woniu.mapper;

import com.woniu.entity.EverApitalflow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
public interface EverApitalflowMapper extends BaseMapper<EverApitalflow> {

}
