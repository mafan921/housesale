package com.woniu.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverHouse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.entity.EverOrder;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface EverHouseMapper extends BaseMapper<EverHouse> {
    IPage<EverHouse> select1(@Param(Constants.WRAPPER) Wrapper<EverHouse> queryWrapper, IPage page);
}
