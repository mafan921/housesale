package com.woniu.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverOrderandsale;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-12-03
 */
public interface EverOrderandsaleMapper extends BaseMapper<EverOrderandsale> {
    IPage<EverOrderandsale> selectSalary( Page<EverOrderandsale> page, @Param(Constants.WRAPPER) Wrapper<EverOrderandsale> wrapper);

    void insertOR(EverOrderandsale oas);


    @Insert(" INSERT INTO ever_orderandsale ( hid,date, `order` ) values (#{hid},#{date},#{order})")
    void insertOrd(@Param("hid") Integer hid,@Param("Date") Date date,@Param("order") BigDecimal order);

    @Select("select * from ever_orderandsale")
    List<EverOrderandsale> selectList123();
}
