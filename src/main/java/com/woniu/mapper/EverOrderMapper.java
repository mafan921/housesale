package com.woniu.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverAppointment;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface EverOrderMapper extends BaseMapper<EverOrder> {

    List<EverOrder> selectOrder();

    List<EverOrder> find(@Param(Constants.WRAPPER) Wrapper<EverOrder> queryWrapper);

    IPage<EverOrder> selectOrderByManager (Page<EverOrder> page, @Param(Constants.WRAPPER) Wrapper<EverOrder> queryWrapper);
    IPage<EverOrder> find2UH(Page<EverOrder> page, @Param(Constants.WRAPPER) Wrapper<EverOrder> wrapper);



//    List<EverOrder> find();
}
