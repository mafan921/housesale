package com.woniu.mapper;

import com.woniu.entity.EverPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface EverPermissionMapper extends BaseMapper<EverPermission> {

}
