package com.woniu.mapper;

import com.woniu.entity.EverEmp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface EverEmpMapper extends BaseMapper<EverEmp> {

}
