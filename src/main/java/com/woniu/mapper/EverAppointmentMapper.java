package com.woniu.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.woniu.entity.EverAppointment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.entity.EverOrder;
import org.apache.ibatis.annotations.Param;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface EverAppointmentMapper extends BaseMapper<EverAppointment> {

//    List<EverAppointment> find2UH(@Param(Constants.WRAPPER) Wrapper<EverAppointment> queryWrapper);

    IPage<EverAppointment> find2UH(Page<EverAppointment> page, @Param(Constants.WRAPPER) Wrapper<EverAppointment> wrapper);
    IPage<EverOrder> selectAppByManager(Page<EverOrder> page,@Param(Constants.WRAPPER)  QueryWrapper queryWrapper);
}
