package com.woniu.mapper;

import com.woniu.entity.EverSeckill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface EverSeckillMapper extends BaseMapper<EverSeckill> {

}
