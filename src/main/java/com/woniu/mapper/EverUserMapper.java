package com.woniu.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-12-03
 */
public interface EverUserMapper extends BaseMapper<EverUser> {
    IPage<EverUser> selectSLByManager(Page<EverUser> page, @Param(Constants.WRAPPER)QueryWrapper<EverUser> queryWrapper);
}
