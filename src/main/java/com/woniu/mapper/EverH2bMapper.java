package com.woniu.mapper;

import com.woniu.entity.EverH2b;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
public interface EverH2bMapper extends BaseMapper<EverH2b> {

}
