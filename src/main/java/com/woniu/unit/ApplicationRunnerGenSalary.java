package com.woniu.unit;


import com.woniu.service.IEverSalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;


@Component
//多个时要添加排序注解，越小越先执行
//@Order(value = 10)
public class ApplicationRunnerGenSalary implements ApplicationRunner {
    @Autowired
    private IEverSalaryService salaryService;

    @Override
    public void run( ApplicationArguments args ) throws Exception {
        salaryService.genSalary();
    }
}
