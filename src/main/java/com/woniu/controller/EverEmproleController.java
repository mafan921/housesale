package com.woniu.controller;


import com.woniu.entity.EverEmp;
import com.woniu.entity.EverEmprole;
import com.woniu.entity.EverRole;
import com.woniu.service.IEverEmpService;
import com.woniu.service.IEverEmproleService;
import com.woniu.service.IEverRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lkl
 * @since 2020-12-04
 */
@RestController
@RequestMapping("/ever-emprole")
@CrossOrigin
public class EverEmproleController {
    @Autowired
    IEverRoleService roleService;
    @Autowired
    IEverEmproleService erService;

            @RequestMapping("/findOneErole")
    public Map<String,Object> findOneErole(Long eid){
                EverEmp emp=new EverEmp();
                emp.setEid(eid);
                Map<String,Object> map=new HashMap<>();
                EverRole oneErole = roleService.findEmpAllRoles(erService.findEmpAllRid(emp));
                    map.put("oneErole",oneErole);
                    return map;
            }
    @RequestMapping("/updateOneErole")
    public Map<String,Object> updateOneErole(Long eid,String rname){

        Map<String,Object> map=new HashMap<>();
        EverEmprole emprole=new EverEmprole();
        Long erid=  erService.findEridByEid(eid);
        Long rid = roleService.findRidByRname(rname);
            emprole.setErid(erid);
            emprole.setEid(eid);
            emprole.setRid(rid);
            erService.updateEmpRole(emprole);

        return map;
    }

    @PostMapping("/save")
    public void save(EverEmprole emprole){
                erService.save(emprole);
    }
}

