package com.woniu.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sun.istack.internal.NotNull;
import com.woniu.entity.EverHouse;
import com.woniu.mapper.EverHouseMapper;
import com.woniu.service.IEverHouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@RestController
@RequestMapping("/ever-house")
@CrossOrigin
public class EverHouseController {

    @Autowired
    private IEverHouseService everHouseService;

    /**
     * 查所有加分页
     * @param size
     * @param current
     * @return
     */
    @GetMapping("findAll/{current}/{size}")
//    @Cacheable(value = "index",key = "'findAll'")
    public List<EverHouse> findAll(@PathVariable Integer current,@PathVariable Integer size){
        return everHouseService.findAll(current,size);
    }

    /**
     * 新增房源
     * @param everHouse
     */
    @PostMapping("save")
//    @CacheEvict(value = "index",key = "'select'")
    public void save(@RequestBody EverHouse everHouse){
        everHouseService.save(everHouse);
    }

    /**
     * 更新房源信息
     * @param everHouse
     */
    @PutMapping("updateById")
//    @CacheEvict(value = "index",key = "'select'")
    public void update(@RequestBody EverHouse everHouse){
        everHouseService.update(everHouse);
    }

    /**
     * 列表删除
     * @param hids
     */
    @DeleteMapping("deleteByList")
//    @CacheEvict(value = "index",key = "'select'")
    public void deleteByList(@RequestParam(value = "hids",required = false) Long[] hids){
        everHouseService.deleteByList(hids);
    }

    /**
     * 模糊查询加分页
     * @param house
     * @param size
     * @param current
     * @return
     */
    @PostMapping("select/{size}/{current}")
//    @Cacheable(value = "index",key = "'select'")
    public List<EverHouse> select(@RequestBody EverHouse house, @PathVariable Integer size,@PathVariable Integer current){
        return everHouseService.selectByWrapper(house,size,current);
    }



}

