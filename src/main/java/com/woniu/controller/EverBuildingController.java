package com.woniu.controller;


import com.woniu.entity.EverBuilding;
import com.woniu.entity.EverHouse;
import com.woniu.service.IEverBuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@RestController
@RequestMapping("/ever-building")
@CrossOrigin
public class EverBuildingController {
    @Autowired
    private IEverBuildingService everBuildingService;

    @GetMapping("findAll")
//    @Cacheable(value = "index",key = "'findAll'")
    public List<EverBuilding> findAll(){
        return everBuildingService.findAll();
    }


}

