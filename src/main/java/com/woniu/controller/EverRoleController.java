package com.woniu.controller;


import com.woniu.entity.EverEmp;
import com.woniu.entity.EverRole;
import com.woniu.service.IEverEmpService;
import com.woniu.service.IEverEmproleService;
import com.woniu.service.IEverRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@RestController
@RequestMapping("/ever-role")
@CrossOrigin
public class EverRoleController {
@Autowired
    IEverEmpService empService;
@Autowired
    IEverEmproleService erService;
@Autowired
    IEverRoleService rServcice;
    @RequestMapping("/findAllEmpsAndErole")
public  Map<String,Object> findAllEmpsAndErole(){
    Map<String, Object> map=new HashMap<>();


        List<EverEmp> everEmps = empService.findAllNoPage();
        for (EverEmp everEmp : everEmps) {
            EverRole role = rServcice.findEmpAllRoles(erService.findEmpAllRid(everEmp));
            everEmp.setRname(role.getRname());
        }
         map.put("empAndRole",everEmps);
        return map;
}
    @RequestMapping("/findAllRoles")
    public  Map<String,Object> findAllRoles(){
        Map<String, Object> map=new HashMap<>();
        List<EverRole> roles = rServcice.findAllRoles();
        map.put("roles",roles);
        return map;
    }
}

