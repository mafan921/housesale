package com.woniu.controller;
import com.woniu.entity.EverEmp;
import com.woniu.entity.EverPermission;
import com.woniu.entity.EverRole;
import com.woniu.service.*;
import com.woniu.service.impl.EverEmproleServiceImpl;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/ever-login")
public class EverLoginController {
    @Autowired
    IEverEmpService empService;
    @Autowired
    IEverEmproleService erService;
    @Autowired
    IEverRoleService rService;
    @Autowired
    IEverRolepermissionService rpService;
    @Autowired
    IEverPermissionService pService;

    @Autowired
    private StringRedisTemplate st;
    /*登录功能
    前端访问对应controller 先判断有没有token
    a.没有token
    1.去认证中心，认证中心判断传来的用户名和密码在数据空中是否存在
    2.不存在的话，直接返回null
    3.存在的话，就加密，并创建一个token传过去
     b.有token的
        1.


    /*
    * 账号密码登录
    * */
   @PostMapping("/accountLogin")
    public Map AccountLogin(@RequestBody EverEmp emp, HttpSession session){
       //获得当前用户
       Subject subject = SecurityUtils.getSubject();
            //封装用户的登录数据
       UsernamePasswordToken token = new UsernamePasswordToken(emp.getEaccount(), emp.getEpwd());
        //执行登录，没有异常就说明登陆成功
       Map<String,Object> map=new HashMap<>();
       try {
           //Shiro自动验证登陆是否正确
           subject.login(token);

           //得到所有权限
           List<EverPermission> roleAllPermissions = pService.findRoleAllPermissions(rpService.findRoleAllPids(rService.findEmpAllRoles(erService.findEmpAllRid(empService.findLoginByAccount(emp.getEaccount())))));
           List<String> list=new ArrayList<>();

           for (EverPermission permission : roleAllPermissions) {
               list.add(permission.getUrl());
           }


           map.put("result",true);
           map.put("role",list);

       } catch (UnknownAccountException e) {
           map.put("result",false);
           System.out.println("账号不存在");
       }catch (IncorrectCredentialsException e) {
           map.put("result",false);
           System.out.println("密码错误");
       }
       return map;
         }


    /**
     * 手机号码登录--给对应的手机号发验证码
      */
//    @Cacheable(value = "index",key = "'findAll'")
    @PostMapping("/phonecode")
    public Map<String ,Object> phonecode(String phone, HttpSession session){
        Map<String ,Object>  map = empService.phonecode(phone,session);
        return  map;
    }
    /**
     * 手机号码登录--前端输入验证码，与session中的对比
     */
//    @CacheEvict(value = "index",key = "'findAll'")
    @PostMapping("/phoneLogin")
    public Map<String ,Object> phoneLogin(String phone,String code, HttpSession session){
        Map<String ,Object>  map = empService.phoneLogin(phone,code,session);
        return map;
    }

}
