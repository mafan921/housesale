package com.woniu.controller;

import com.woniu.entity.EverOrder;
import com.woniu.entity.EverSalerecord;
import com.woniu.log.AnnotationLog;
import com.woniu.service.SelfSalesrecordService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author : mafan
 * @date : 2020-12-02 20:32
 **/
@RestController
@RequestMapping("/self/salerecord")
@CrossOrigin
public class SelfSalesrecordController {

    @Resource
    private SelfSalesrecordService salesrecordService;

    /**
     * 查看所有销售记录
     * @return
     */
    @PostMapping("find")
    public Map<String, Object> findAll(Integer current, Integer size) {
        return salesrecordService.findAllSalrecord(current, size);
    }
    /**
     * 根据订单
     *      生成销售记录
     * @param order
     * @return
     */
    @AnnotationLog(descrption = "楼盘:支付尾款")
    @PostMapping("saveByOrd")
    public Map<String, Object>  saveSalesrecord(@RequestBody EverOrder order){
        Map<String, Object> resultMap = new HashMap<>();
        try {
            salesrecordService.saveByOrd(order);
            resultMap.put("result", true);
        } catch (Exception e) {
            resultMap.put("result",false);
        }
        return resultMap;
    }

    /**
     * 根据id删除
     * @param saleid
     * @return
     */
    @PostMapping("delete")
    public Map<String, Object>  deleteSalesrecord(@PathVariable Long saleid){
        Map<String, Object> resultMap = new HashMap<>();
        try {
            salesrecordService.deleteBySaleid(saleid);
            resultMap.put("result", true);
        } catch (Exception e) {
            resultMap.put("result",false);
        }
        return resultMap;
    }

    /**
     * 修改记录
     * @param salerecord
     * @return
     */
    @PostMapping("update")
    public Map<String, Object>  updateSalesrecord(@PathVariable EverSalerecord salerecord){
        Map<String, Object> resultMap = new HashMap<>();
        try {
            salesrecordService.updateBySaleid(salerecord);
            resultMap.put("result", true);
        } catch (Exception e) {
            resultMap.put("result", false);
        }
        return resultMap;
    }

}
