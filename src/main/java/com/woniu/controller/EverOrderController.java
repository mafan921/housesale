package com.woniu.controller;


import com.woniu.entity.EverOrder;
import com.woniu.log.AnnotationLog;
import com.woniu.service.IEverOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@RestController
@RequestMapping("/ever-order")
@CrossOrigin  //跨域
public class EverOrderController {

    @Autowired
    private IEverOrderService everOrderService;

    /**
     * 经理查询所有订单
     * @param order
     * @param pageNo
     * @param pageSize
     * @return
     */
    @AnnotationLog(descrption = "订单:查询了所有订单")
    @PostMapping("select/{pageNo}/{pageSize}")
    public Map<String,Object> findAllAndSearch(@RequestBody EverOrder order,
                                               @PathVariable Integer pageNo,
                                               @PathVariable Integer pageSize){
        Map<String,Object> result=everOrderService.findAllAndSearch(order,pageNo,pageSize);

        return result;
    }
}

