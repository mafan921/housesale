package com.woniu.controller;


import com.woniu.entity.EverAppointment;
import com.woniu.entity.EverOrder;
import com.woniu.service.IEverAppointmentService;
import org.apache.ibatis.binding.BindingException;
import org.omg.CORBA.DynAnyPackage.Invalid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLSyntaxErrorException;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@RestController
@RequestMapping("/ever-appointment")
@CrossOrigin
public class EverAppointmentController {
    @Autowired
    private IEverAppointmentService iEverAppointmentService;
//    Invalid bound statement
//    SQLSyntaxErrorException   Redirect
    /**
     * 经理查询所有预约
     * @param everAppointment
     * @param pageNo
     * @param pageSize
     * @return
     */
    @PostMapping("ManagerSelectAll/{pageNo}/{pageSize}")
    private Map<String,Object > ManagerSelectAll(@RequestBody EverAppointment everAppointment,
                                                 @PathVariable Integer pageNo,
                                                 @PathVariable Integer pageSize){

        return iEverAppointmentService.ManagerSelectAllByPage(everAppointment, pageNo, pageSize);


    }

    /**
     * 经理添加一条预约
     * @return
     */
    private Map<String, Object> ManagerAdd(EverAppointment everAppointment){
        return  null;//iEverAppointmentService.insertOneApp(everAppointment);
    }
}

