package com.woniu.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverAppointment;
import com.woniu.entity.EverOrder;
import com.woniu.log.AnnotationLog;
import com.woniu.service.SelfApptService;
import com.woniu.service.SelfSalesrecordService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author : mafan
 * @date : 2020-11-30 17:29
 **/
@RestController
@RequestMapping("/self/appt")
@CrossOrigin
public class SelfApptController {

    @Resource
    private SelfApptService appointmentService;
    @Resource
    private SelfSalesrecordService salesrecordService;

    /**
     * 查看所有预约记录
     * @author : mafan
     * @return
     */
    @PostMapping("find")
    public Map<String, Object> findAll(Integer current, Integer size,@RequestBody EverAppointment appointment){
        System.out.println("=========================="+appointment);
        System.out.println("页码："+current+"条数："+size);
        return appointmentService.findAllAppt(current,size);
    }

    /**
     *  根据id  查出预约记录（时间大的在上）  分页
     * @param eid
     * @param current
     * @param size
     * @return
     */
    @GetMapping("findByEid/{eid}/{current}/{size}")
    public Page<EverAppointment> findByEid(@PathVariable Long eid,
                                           @PathVariable Integer current,
                                           @PathVariable Integer size ){
        Page<EverAppointment> appointmentPage = appointmentService.findByEid(eid, current, size);
        return appointmentPage;
    }
    /**
     * 根据预约
     *      生成销售记录
     * @param appointment
     * @return
     */
    @AnnotationLog(descrption = "楼盘:全款支付")
    @PostMapping("saveRecord")
    public Map<String, Object>  saveRecord(@RequestBody EverAppointment appointment){
        Map<String, Object> resultMap = new HashMap<>();
        try {
            salesrecordService.saveRecord(appointment);
            resultMap.put("result", true);
        } catch (Exception e) {
            resultMap.put("result",false);
        }
        return resultMap;
    }


    /**
     * 新增预约信息   @ApiOperation("新增预约信息")
     * @param appointment
     */
    @AnnotationLog(descrption = "楼盘:新增一条预约信息")
    @PostMapping("saveAppt")
    public void saveAppt(@Validated @RequestBody EverAppointment appointment){
        System.out.println("SelfApptController.saveAppt"+appointment);
        //预约时间
        appointment.setAppstate("预约");
        appointment.setEid((long) 1);
      //save of update
       appointmentService.saveOfupdate(appointment);
    }

    /**
     * 修改预约信息   @ApiOperation("修改预约信息")
     * @param appointment
     */
    @AnnotationLog(descrption = "楼盘:修改预约状态")
    @PostMapping("updateAppt")
    public void upAppt(@Validated @RequestBody EverAppointment appointment){
        appointmentService.saveOfupdate(appointment);
        System.out.println(appointment);
    }

    /**
     * 删除预约信息   @ApiOperation("删除预约信息")
     * @return
     */
    @DeleteMapping("delAppt/{aid}")
    public String delAppt(@PathVariable Long aid){
        appointmentService.deleteAppt(aid);
        return "";
    }

}
