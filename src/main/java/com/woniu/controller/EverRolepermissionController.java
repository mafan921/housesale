package com.woniu.controller;


import com.woniu.entity.EverEmp;
import com.woniu.entity.EverPermission;
import com.woniu.entity.EverRole;
import com.woniu.service.IEverPermissionService;
import com.woniu.service.IEverRolepermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
@RestController
@RequestMapping("/ever-rolepermission")
@CrossOrigin
public class EverRolepermissionController {
    @Autowired
    IEverRolepermissionService rpService;
    @Autowired
    IEverPermissionService permissionService;
            @RequestMapping("/findRolePermissions")
        public Map<String,Object> findRolePermissions( Long rid){
                EverRole role=new EverRole();
                role.setRid(rid);
                Map<String,Object> map=new HashMap<>();
                List<Long> pids = rpService.findRoleAllPids(role);

             List<EverPermission> oneRolePermissions=permissionService.findPermissionByPids(pids);

             map.put("oneRolePermissions",oneRolePermissions);
             return map;
            }
            @RequestMapping("saveRoleNewPermissions")
    public  Map<String,Object> saveRoleNewPermissions(@RequestBody List<Long> pids){

                Map<String,Object> map=new HashMap<>();
                return map;
            }

}

