package com.woniu.controller;


import com.woniu.entity.EverSalerecord;
import com.woniu.entity.EverUser;
import com.woniu.service.IEverSalaryService;
import com.woniu.service.IEverSalerecordService;
import com.woniu.service.SelfOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@RestController
@RequestMapping("/ever-salerecord")
@CrossOrigin
public class EverSalerecordController {
    @Autowired
    private IEverSalerecordService iEverSalerecordService;


    @PostMapping("/ManagerFindAllSL/{pageNo}/{pageSize}")
    private Map<String, Object> ManagerFindAllSL(@RequestBody(required = false)EverSalerecord salerecord,
                                                 @PathVariable Integer pageNo,
                                                 @PathVariable Integer pageSize){
        return iEverSalerecordService.ManagerFindAllSL(salerecord, pageNo, pageSize);

    }

    @PostMapping("reportMoney")
    public List<EverSalerecord> reportMoney(@RequestBody(required = false)EverSalerecord salerecord){
        return iEverSalerecordService.reportMoney(salerecord);
    }



}

