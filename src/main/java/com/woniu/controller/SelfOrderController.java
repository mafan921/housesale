package com.woniu.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.common.JsonResult;
import com.woniu.entity.EverAppointment;
import com.woniu.entity.EverOrder;
import com.woniu.log.AnnotationLog;
import com.woniu.service.SelfOrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : mafan
 * @date : 2020-12-01 21:08
 **/
@RestController
@RequestMapping("/self/order")
@CrossOrigin
public class SelfOrderController {

    @Resource
    private SelfOrderService orderService;

    /**
     *  查看订单
     * @param current
     * @param size
     * @return
     */
    @PostMapping("find")
    public Map<String, Object> findAll(Integer current, Integer size){
        return orderService.findAllorder(current,size);
    }

    /**
     * 查询职员的所有订单
     *
     * @param eid
     */
    @GetMapping("findByEid/{eid}")
    public JsonResult findByEid(@PathVariable Long eid) {
        try {
            orderService.findtest(eid);
            return JsonResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JsonResult.ok();
    }


    /**
     * 查询所有订单
     *
     * @return
     */
    @GetMapping("findAll")
    public Map<String, Object> findAll() {
        Map<String, Object> resultMap = new HashMap<>();
        try {
            List<EverOrder> orderList = orderService.findAllOrder();
            resultMap.put("ordList", orderList);
            resultMap.put("result", true);
        } catch (Exception e) {
            resultMap.put("result", false);
        }
        return resultMap;
    }

    /**
     * 新增订单
     *
     * @param order
     */
    @PostMapping("save")
    public Map<String, Object> saveOrder(@RequestBody EverOrder order) {
        Map<String, Object> resultMap = new HashMap<>();
        try {
            orderService.saveOrder(order);
            resultMap.put("result", true);
        } catch (Exception e) {
            resultMap.put("result", false);
        }
        return resultMap;
    }

    /**
     * 根据预约单
     * 生成订单
     *
     * @param appointment
     * @return
     */
    @AnnotationLog(descrption = "楼盘:预约单生成订单")
    @PostMapping("saveByAppt")
    public Map<String, Object> saveOrder(@RequestBody EverAppointment appointment) {
        Map<String, Object> resultMap = new HashMap<>();
        try {
            System.out.println("生成订单"+appointment);
            orderService.saveByAppt(appointment);
            resultMap.put("result", true);
        } catch (Exception e) {
            resultMap.put("result", false);
        }
        return resultMap;
    }


    /**
     * 删除订单
     * 根据id删除
     */
    @PostMapping("delete")
    public Map<String, Object> deleteOrder(@PathVariable Long ordid) {
        Map<String, Object> resultMap = new HashMap<>();
        try {
            orderService.deleteByOrdid(ordid);
            resultMap.put("result", true);
        } catch (Exception e) {
            resultMap.put("result", false);
        }
        return resultMap;
    }

    /**
     * 修改订单
     */
    @PostMapping("update")
    public Map<String, Object> update(@PathVariable EverOrder order) {
        Map<String, Object> resultMap = new HashMap<>();
        try {
            orderService.updateByOrdid(order);
            resultMap.put("result", true);
        } catch (Exception e) {
            resultMap.put("result", false);
        }
        return resultMap;
    }

}
