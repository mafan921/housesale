package com.woniu.controller;
import com.woniu.entity.EverEmp;
import com.woniu.entity.EverHouse;
import com.woniu.service.IEverEmpService;
import org.apache.tools.ant.taskdefs.condition.Http;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * @author lkl
 * @since 2020-11-29
 */
@RestController
@RequestMapping("/ever-emp")
@CrossOrigin
public class EverEmpController {
    @Autowired
    private IEverEmpService everEmpService;

    /**
     * 查询所有员工信息
     *
     * @return
     */
    @GetMapping("findAll/{current}/{size}")
//    @Cacheable(value = "index", key = "'findAll'")
    public List<EverEmp> findAll(@PathVariable Integer current,@PathVariable Integer size) {
        return everEmpService.findAll(current,size);
    }

    /**
     * 修改某个员工信息
     *
     * @param everEmp
     * @return
     */
    @PutMapping("update")
//    @CacheEvict(value = "index", key = "'findAll'")
    public Integer updateById(@RequestBody EverEmp everEmp) {
        return everEmpService.updateById(everEmp);
    }

    /**
     * 新增员工信息
     *
     * @param everEmp
     */
    @PostMapping("save")
//    @CacheEvict(value = "index", key = "'findAll'")
    public void save(@RequestBody EverEmp everEmp) {
        everEmpService.save(everEmp);
    }

    /**
     * 删除员工信息(批量删除和单独删除);
     * @param empLists
     */
    @DeleteMapping("deleteByList")
//    @CacheEvict(value = "index", key = "'findAll'")
    public void deleteByList(@RequestParam(value = "empLists",required = false) Long[] empLists) {
            everEmpService.delByList(empLists);
    }


    /**
     * 模糊查询加分页
     * @param emp
     * @param size
     * @param current
     * @return
     */
    @PostMapping("select/{size}/{current}")
//    @Cacheable(value = "index",key = "'select'")
    public List<EverEmp> select(@RequestBody EverEmp emp, @PathVariable Integer size, @PathVariable Integer current){
        return everEmpService.selectByWrapper(emp,size,current);
    }

    @GetMapping("findAllNoPage")
//    @Cacheable(value = "index", key = "'findAll'")
    public List<EverEmp> findAllNoPage() {
        return everEmpService.findAllNoPage();
    }

    @PostMapping("myself")
    public Map<String, Object> myself(HttpSession session,Integer eid){
        return everEmpService.findMyself(session, eid);
    }

    @PostMapping("updatePwd")
    public Map<String, Object> updatePwd(@RequestBody EverEmp emp){
        return everEmpService.updatePwd(emp);
    }


}

