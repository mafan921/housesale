package com.woniu.controller;


import com.woniu.entity.EverApitalflow;
import com.woniu.entity.EverOrderandsale;
import com.woniu.entity.EverSalerecord;
import com.woniu.log.AnnotationLog;
import com.woniu.service.IEverApitalflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
@RestController
@RequestMapping("/ever-apitalflow")
@CrossOrigin  //跨域
public class EverApitalflowController {
    @Autowired
    private IEverApitalflowService apitalflowService;

    /**
     * 查看工资总账
     * @return
     */
    @AnnotationLog(descrption = "资金:查询公司总账")
    @GetMapping("find")
    public EverApitalflow find(){
        return apitalflowService.find();
    }

    /**
     * 查询目标值
     * @return
     */
    @AnnotationLog(descrption = "资金:查询每日工资目标值")
    @GetMapping("glosal")
    public EverApitalflow glosal(EverApitalflow ap){
        return apitalflowService.glosal(ap);
    }
    /**
     * 根据所传参数 决定查询周期
     * @return
     */
    @AnnotationLog(descrption = "资金:查询每日具体资金流水")
    @GetMapping("findEvery")
    public List<EverOrderandsale> findEvery( Integer day,
                                             Date starttime,
                                             Date endtime,
                                             Integer current,
                                             Integer size
    ){
        return apitalflowService.findEvery(day,starttime,endtime,current,size);
    }
}

