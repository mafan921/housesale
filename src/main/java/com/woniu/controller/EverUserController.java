package com.woniu.controller;


import com.woniu.entity.EverUser;
import com.woniu.log.AnnotationLog;
import com.woniu.service.IEverUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@RestController
@RequestMapping("/ever-user")
@CrossOrigin  //跨域
public class EverUserController {

    @Autowired
    private IEverUserService userService;
    private Boolean isSerach=true;


    /**
     * 修改一个用户信息
     */
    @AnnotationLog(descrption = "客户:修改了客户信息")
    @PutMapping("update")
    //修改一个要清除redis中所有的的缓存
    //@CacheEvict(value = "user",key = "'findAll'")
    public Integer updateOne(@RequestBody EverUser user,@RequestParam("uid") Long uid) throws Exception {
        user.setUid(uid);
        return userService.updateOne(user);
    }

    /**
     * 查询单个用户
     */
    @GetMapping("findOne")
    public Map<String, Object> findOne(@RequestParam("uid") Long uid) throws Exception {
        EverUser user=new EverUser();
        user.setUid(uid);
        return userService.findOne(user);
    }

    /**
     * 增加用户
     */
    @PostMapping("addUser")
    @AnnotationLog(descrption = "客户:添加了一个客户")
    public String addUser(@RequestBody EverUser user) throws InterruptedException {
        String s = userService.addUser(user);
        if (s.equals("success")){
            isSerach=true;
            return "添加用户成功";
        }
        return s;
    }

    /**
     * 根据id删除用户
     */
    @AnnotationLog(descrption = "客户:删除了一个客户")
    @DeleteMapping("deleteById")
    public String deleteById(@PathParam("uid") Long uid) throws Exception {
        String result=userService.deleteById(uid);
        if (result.equals("ok")){
            return result;
        }
        return "nook";
    }


    /**
     * 查询所有用户and模糊查询
     */
    @AnnotationLog(descrption = "客户:查询了所有客户")
    @PostMapping("/serach/{pageNo}/{pageSize}")
    public Map<String,Object> search(@RequestBody(required = false) EverUser user,
                                           @PathVariable Integer pageNo,
                                           @PathVariable Integer pageSize) throws Exception {
        if (isSerach){
            isSerach=false;
            userService.insertES();
        }
        return userService.serachPageHighLight(user, pageNo, pageSize);

    }

    /**
     * 查询所有用户
     */
    @GetMapping("/searchAll")
    public List<EverUser> search(@RequestBody EverUser user){
        return userService.searchAll();
    }

    @GetMapping("/rua")
    //添加数据
    public  void insert(){
        List<EverUser> list=new ArrayList<>();
        list.add(new EverUser("张无忌","admin1" ,"123456" ,"15591991567" ,"男" ,"1234561" ,1 ,"北京" ));
        list.add(new EverUser("陆逊","admin2" ,"123456" ,"15591992567" ,"男" ,"1234526" ,1 ,"北京" ));
        list.add(new EverUser("司马懿","admin3" ,"123456" ,"15591934567" ,"男" ,"1234563" ,1 ,"北京" ));
        list.add(new EverUser("司马昭","admin4" ,"123456" ,"15591944567" ,"男" ,"1234564" ,1 ,"北京" ));
        list.add(new EverUser("司马光","admin5" ,"123456" ,"15591954567" ,"男" ,"1234565" ,1 ,"北京" ));
        list.add(new EverUser("关羽","admin6" ,"123456" ,"15591996567" ,"男" ,"1234566" ,1 ,"北京" ));
        list.add(new EverUser("关胜","admin7" ,"123456" ,"15591997567" ,"男" ,"1234567" ,1 ,"北京" ));
        list.add(new EverUser("关兴","admin8" ,"123456" ,"15591998567" ,"男" ,"1234568" ,1 ,"北京" ));
        list.add(new EverUser("宋江","admin9" ,"123456" ,"15591999567" ,"男" ,"1234569" ,1 ,"北京" ));
        list.add(new EverUser("扈三娘","admin0" ,"123456" ,"15591904567" ,"男" ,"1234516" ,1 ,"北京" ));
        list.add(new EverUser("孙二娘","adminq" ,"123456" ,"15591994567" ,"男" ,"1234156" ,1 ,"北京" ));
        list.add(new EverUser("李逵","adminw" ,"123456" ,"15592994567" ,"男" ,"1231456" ,1 ,"北京" ));
        list.add(new EverUser("李鬼","admine" ,"123456" ,"15593994567" ,"男" ,"12342563" ,1 ,"北京" ));
        list.add(new EverUser("张顺","adminr" ,"123456" ,"15594994567" ,"男" ,"12345632" ,1 ,"北京" ));
        list.add(new EverUser("阮小二","admint" ,"123456" ,"15544994567" ,"女" ,"12345236" ,1 ,"北京" ));
        list.add(new EverUser("阮小五","adminy" ,"123456" ,"15591994567" ,"女" ,"12342356" ,1 ,"北京" ));
        list.add(new EverUser("阮小七","adminu" ,"123456" ,"15591994567" ,"女" ,"12323456" ,1 ,"北京" ));
        list.add(new EverUser("林黛玉","admini" ,"123456" ,"15591994567" ,"女" ,"12233456" ,1 ,"北京" ));
        list.add(new EverUser("卢俊义","admino" ,"123456" ,"15591994567" ,"女" ,"12323456" ,1 ,"北京" ));
        list.add(new EverUser("燕青","adminp" ,"123456" ,"15591994567" ,"女" ,"123456123" ,1 ,"北京" ));
        list.add(new EverUser("于禁","admina" ,"123456" ,"15591994567" ,"女" ,"123456132" ,1 ,"北京" ));
        list.add(new EverUser("岳云鹏","admins" ,"123456" ,"15591994567" ,"女" ,"123456321" ,1 ,"北京" ));
        list.add(new EverUser("郭德纲","admind" ,"123456" ,"15591994567" ,"女" ,"123456213" ,1 ,"北京" ));
        list.add(new EverUser("夏洛特","adminf" ,"123456" ,"15591994567" ,"女" ,"123456345" ,1 ,"北京" ));
        list.add(new EverUser("马冬梅","adming" ,"123456" ,"15591994567" ,"女" ,"123456453" ,1 ,"北京" ));
        list.add(new EverUser("周芷若","adminh" ,"123456" ,"15591994567" ,"女" ,"123456543" ,1 ,"北京" ));
        list.add(new EverUser("谢逊","adminj" ,"123456" ,"15591994567" ,"女" ,"123456534" ,1 ,"北京" ));
        list.add(new EverUser("灭绝师太","admink" ,"123456" ,"15591994567" ,"女" ,"123456566" ,1 ,"北京" ));
        list.add(new EverUser("小芷昭","adminz" ,"123456" ,"15591994567" ,"女" ,"123456765" ,1 ,"北京" ));
        list.add(new EverUser("太逊芷","adminx" ,"123456" ,"15591994567" ,"女" ,"1234566766" ,1 ,"北京" ));
        userService.insertList(list);

    }
}

