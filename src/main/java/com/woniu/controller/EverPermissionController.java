package com.woniu.controller;


import com.woniu.entity.EverPermission;
import com.woniu.service.IEverPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@RestController
@RequestMapping("/ever-permission")
@CrossOrigin
public class EverPermissionController {
    @Autowired
    IEverPermissionService permissionService;
            @RequestMapping("/findAllPermissions")
        public Map<String, Object> findAllPermissions(){
            Map<String, Object> map=new HashMap<>();
          List<EverPermission>permissions=  permissionService.findAllPermissions();
          map.put("permissions",permissions);

            return map;
        }
}

