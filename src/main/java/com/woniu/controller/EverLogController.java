package com.woniu.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverLog;
import com.woniu.entity.EverSalary;
import com.woniu.service.IEverLogService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@RestController
@RequestMapping("/ever-log")
@CrossOrigin  //跨域
public class EverLogController {
    @Resource
    private IEverLogService logService;

    /**
     * 查询所有 日志
     * @return
     */
    @GetMapping("findAll")
    public Map<String,Object> findOne(
                                Date starttime,
                                Date endtime,
                                Integer current,
                                Integer size,
                                String ename,
                                String yl
    ){
        Page<EverLog> all = logService.findAll(starttime, endtime, current, size, ename, yl);
        List<EverLog> records = all.getRecords();
        if(records.size()>0){
        records.get(0).setSum((int)all.getTotal());}
        Map<String,Object> map = new HashMap<>();
        map.put("logList",records);
        List<String> style = logService.findStyle();
        map.put("style",style);
        return map;
    }
}

