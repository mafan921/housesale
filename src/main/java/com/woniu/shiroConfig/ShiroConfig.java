package com.woniu.shiroConfig;

import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {
        @Bean
    //ShiroFilterFactoryBean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("securityManager")DefaultWebSecurityManager defaultWebSecurityManager){

        ShiroFilterFactoryBean bean=new ShiroFilterFactoryBean();
        //设置安全管理器
        bean.setSecurityManager(defaultWebSecurityManager);
        //添加shiro内置过滤器
        /*
        * anon：无需认证就可访问
        * authc:必须认证才能访问
        * user：必须拥有记住我才能访问
        * perms：拥有对某个资源的权限才能访问
        * role：拥有某个角色权限才能访问
        *
        * */
        Map<String,String> filterMap=new LinkedHashMap<>();
            System.out.println("???????????????????????????????????");
        //拦截所有请求
//        filterMap.put("/ever-permission/findAllPermissions","authc,roles[admin]");
        //拦截 /add请求，需要xxx权限，通过该emp找到对应的权限
//        filterMap.put("/add","perms[]");

//        filterMap.put("/*","authc")
        bean.setFilterChainDefinitionMap(filterMap);

                    //设置登陆的请求
        bean.setLoginUrl("/ever-login/accountLogin");
        //设置未授权
//        bean.setUnauthorizedUrl("/");
        return bean;
    }
    //DefaultWebSecurityManager
    @Bean(name="securityManager")
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("empRealm")EmpRealm empRealm){
        System.out.println("4444444444444444444444444444444444");
        DefaultWebSecurityManager securityManager=new DefaultWebSecurityManager();
        //关联Realm
        securityManager.setRealm(empRealm);
        return securityManager;
    }

  //realm
    @Bean
public EmpRealm  empRealm(){
        System.out.println("33333333333333333333333333");
        return new EmpRealm();
    }
}
