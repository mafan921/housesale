package com.woniu.service;

import com.woniu.entity.EverOrder;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface IEverOrderService extends IService<EverOrder> {

    Map<String, Object> findAllAndSearch(EverOrder order, Integer pageNo, Integer pageSize);
}
