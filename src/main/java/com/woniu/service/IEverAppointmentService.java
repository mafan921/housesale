package com.woniu.service;

import com.woniu.entity.EverAppointment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface IEverAppointmentService  {

    Map<String, Object> ManagerSelectAllByPage(EverAppointment everAppointment, Integer pageNo, Integer pageSize);

}
