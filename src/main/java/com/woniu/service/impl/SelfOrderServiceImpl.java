package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.entity.EverAppointment;
import com.woniu.entity.EverHouse;
import com.woniu.entity.EverOrder;
import com.woniu.entity.EverOrderandsale;
import com.woniu.mapper.EverAppointmentMapper;
import com.woniu.mapper.EverHouseMapper;
import com.woniu.mapper.EverOrderMapper;
import com.woniu.mapper.EverOrderandsaleMapper;
import com.woniu.service.SelfOrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : mafan
 * @date : 2020-12-01 21:10
 **/
@Service
public class SelfOrderServiceImpl extends ServiceImpl<EverOrderMapper, EverOrder> implements SelfOrderService {

    @Resource
    private EverAppointmentMapper appointmentMapper;
    @Resource
    private EverOrderMapper orderMapper;
    @Resource
    private EverHouseMapper houseMapper;
    @Resource
    private EverOrderandsaleMapper orderandsaleMapper;

    @Override
    public List<EverOrder> findtest(Long eid) {
        List<EverOrder> everOrders = orderMapper.find(new QueryWrapper<EverOrder>()
                .eq("eid", "eid")
                .orderByDesc("datetime"));
        everOrders.forEach(System.out::println);
        System.out.println(123);
        return everOrders;
    }

    @Override
    public List<EverOrder> findAllOrder() {
        QueryWrapper<EverOrder> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("datetime");
        List<EverOrder> orderList = orderMapper.selectList(wrapper);
        orderList.forEach(System.out::println);
        return orderList;
    }

    @Override
    public void saveOrder(EverOrder order) {
        orderMapper.insert(order);
    }

    @Override
    public void deleteByOrdid(Long ordid) {
        orderMapper.deleteById(ordid);
    }

    @Override
    public void updateByOrdid(EverOrder order) {
        orderMapper.updateById(order);
    }

    @Override
    public void saveByAppt(EverAppointment appointment) {
        //先根据预约单查询出所有信息
        QueryWrapper<EverAppointment> wrapper = new QueryWrapper<>();
        wrapper.eq("aid", appointment.getAid());
        List<EverAppointment> appointmentList = appointmentMapper.selectList(wrapper);
        EverAppointment appt = appointmentList.get(0);
        //先new一个订单然后把appt的数据取出来放到订单中
        EverOrder order = new EverOrder();
        order.setEid(appt.getEid());
        order.setHid(appt.getHid());
        order.setUid(appt.getUid());
        order.setOrderstate("已交定金");
        order.setDatetime(new Date());
        order.setOsal("50000");
        //订金根据房子总价的百分之几
//        EverHouse everHouse = houseMapper.selectById(appointment.getHid());
        //将订单存入数据库
        orderMapper.insert(order);
        //软删除房屋
        EverHouse house = new EverHouse();
        house.setHid(appt.getHid());
        house.setSoftdel(0);
        houseMapper.updateById(house);
        //删除预约表中这行数据
        appointmentMapper.deleteById(appointment.getAid());
        //往ever_orderandsale存资金变化  订金
        EverOrderandsale oas = new EverOrderandsale();
        oas.setDate(new Date());
        oas.setHid(appt.getHid().intValue());
        BigDecimal a= BigDecimal.valueOf(50000.00);
        oas.setOrder(a);
//        orderandsaleMapper.insertOrd(oas.getHid(),oas.getDate(),oas.getOrder());
        orderandsaleMapper.insertOR(oas);
    }

    @Override
    public Map<String, Object> findAllorder(Integer current, Integer size) {
        System.out.println("页码："+current+"条数："+size);
        Page<EverOrder> page = new Page<>(current, size);
        //wapper
        QueryWrapper<EverOrder> wrapper = new QueryWrapper<>();
        wrapper
                .eq("eid", "1")
                .orderByDesc("datetime");
        //find2UH order.xml中的方法
        IPage<EverOrder> orders = orderMapper.find2UH(page,wrapper);
        List<EverOrder> orderList = orders.getRecords();
        long total = orders.getTotal();
        Map<String, Object> map=new HashMap<>();
        map.put("list", orderList);
        map.put("total", total);
        return map;
    }
}
