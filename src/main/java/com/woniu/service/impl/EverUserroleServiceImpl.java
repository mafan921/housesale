package com.woniu.service.impl;

import com.woniu.entity.EverUserrole;
import com.woniu.mapper.EverUserroleMapper;
import com.woniu.service.IEverUserroleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
@Service
public class EverUserroleServiceImpl extends ServiceImpl<EverUserroleMapper, EverUserrole> implements IEverUserroleService {

}
