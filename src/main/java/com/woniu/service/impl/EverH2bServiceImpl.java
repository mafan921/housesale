package com.woniu.service.impl;

import com.woniu.entity.EverH2b;
import com.woniu.mapper.EverH2bMapper;
import com.woniu.service.IEverH2bService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
@Service
public class EverH2bServiceImpl extends ServiceImpl<EverH2bMapper, EverH2b> implements IEverH2bService {

}
