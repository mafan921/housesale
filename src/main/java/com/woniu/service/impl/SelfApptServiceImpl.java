package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.entity.EverAppointment;
import com.woniu.entity.EverUser;
import com.woniu.mapper.EverAppointmentMapper;
import com.woniu.mapper.EverUserMapper;
import com.woniu.service.SelfApptService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : mafan
 * @date : 2020-12-01 21:02
 **/
@Service
public class SelfApptServiceImpl extends ServiceImpl<EverAppointmentMapper, EverAppointment> implements SelfApptService {

    @Resource
    private EverAppointmentMapper appointmentMapper;
    @Resource
    private EverUserMapper userMapper;

    @Override
    public Page<EverAppointment> findByEid(Long eid, Integer current, Integer size) {
        return null;
    }

    @Override
    public void saveOfupdate(EverAppointment appointment) {
        if (appointment.getAid() == null) {
            System.out.println("SelfApptServiceImpl.saveOfupdate"+appointment);
            //先按照uname查询如果有就返回uid，没有就新增
            QueryWrapper<EverUser> wrapper = new QueryWrapper<>();
            wrapper.eq("uname", appointment.getUname());
            List<EverUser> userList = userMapper.selectList(wrapper);
            if (userList.size()==0){
                //先新增user
                EverUser u = new EverUser();
                u.setUname(appointment.getUname());
                u.setPhone(appointment.getPhone());
                userMapper.insert(u);
                //再查询
                QueryWrapper<EverUser> wrapper2 = new QueryWrapper<>();
                wrapper2.eq("uname", u.getUname());
                List<EverUser> userList2 = userMapper.selectList(wrapper2);
                EverUser user2 = userList2.get(0);
                //将uid存入appt中
                appointment.setUid(user2.getUid());
                appointmentMapper.insert(appointment);

            }else{
                EverUser user = userList.get(0);
                System.out.println("用户uid"+user.getUid());
                appointment.setUid(user.getUid());
                appointmentMapper.insert(appointment);

            }
        } else {
            appointmentMapper.updateById(appointment);
        }
    }

    @Override
    public void deleteAppt(Long aid) {

    }

    @Override
    public Map<String, Object> findAllAppt(Integer current, Integer size) {
        System.out.println("页码："+current+"条数："+size);
        Page<EverAppointment> page = new Page<>(current, size);
        //wapper
        QueryWrapper<EverAppointment> wrapper = new QueryWrapper<>();
        wrapper
//                .eq("eid", "1")
                .orderByDesc("begintime");
        //find2UH
        IPage<EverAppointment> appointments = appointmentMapper.find2UH(page,wrapper);
        List<EverAppointment> appointmentList = appointments.getRecords();

        long total = appointments.getTotal();
        Map<String, Object> map=new HashMap<>();
        map.put("list", appointmentList);
        map.put("total", total);
        return map;
    }

}
