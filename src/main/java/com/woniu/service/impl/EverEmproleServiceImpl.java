package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.entity.EverEmp;
import com.woniu.entity.EverEmprole;
import com.woniu.mapper.EverEmproleMapper;
import com.woniu.service.IEverEmproleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-12-04
 */
@Service
public class EverEmproleServiceImpl extends ServiceImpl<EverEmproleMapper, EverEmprole> implements IEverEmproleService {
        @Autowired
        EverEmproleMapper erMapper;
        @Override
        public Long findEmpAllRid(EverEmp emp) {
            QueryWrapper<EverEmprole> qw=new QueryWrapper();

            qw.eq("eid",emp.getEid());
            List<EverEmprole> ers = erMapper.selectList(qw);


            return ers.size()!=0?ers.get(0).getRid():null;
        }

    @Override
    public Long findEmpAllRidByAccount(EverEmp emp) {
        QueryWrapper<EverEmprole> qw=new QueryWrapper();

        qw.eq("eaccount",emp.getEaccount());
        List<EverEmprole> ers = erMapper.selectList(qw);


        return ers.size()!=0?ers.get(0).getRid():null;
    }



    @Override
    public void updateEmpRole(EverEmprole emprole) {
        QueryWrapper<EverEmprole> qw=new QueryWrapper();
        erMapper.updateById(emprole);
    }

    @Override
    public Long findEridByEid(Long eid) {
        QueryWrapper<EverEmprole> qw=new QueryWrapper();

        qw.eq("eid",eid);
        List<EverEmprole> emproles = erMapper.selectList(qw);


        return emproles.size()!=0?emproles.get(0).getErid():null;
    }

    @Override
    public boolean save(EverEmprole emprole) {
            erMapper.insert(emprole);
        return false;
    }


}
