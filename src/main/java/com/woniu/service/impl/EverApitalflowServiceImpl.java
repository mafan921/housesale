package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.*;
import com.woniu.mapper.*;
import com.woniu.service.IEverApitalflowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
@Service
@Transactional
public class EverApitalflowServiceImpl extends ServiceImpl<EverApitalflowMapper, EverApitalflow> implements IEverApitalflowService {
    @Resource
    private EverApitalflowMapper apitalflowMapper;
    @Resource
    private EverOrderMapper orderMapper;
    @Resource
    private EverSalerecordMapper salerecordMapper;
    @Resource
    private EverSalaryMapper salaryMapper;
    @Resource
    private EverOrderandsaleMapper orderandsaleMapper;

    /**
     * 查询公司总账
     * @return
     */
    @Override
    public EverApitalflow find() {
        //总司账号总金额  （旧）
        EverApitalflow apitalflow = apitalflowMapper.selectList(null).get(0);
        //定金产生的总金额
        List<EverOrder> orderList = orderMapper.selectList(null);
        Integer reduce = orderList.stream().map(EverOrder::getOsal).map(b -> Integer.parseInt(b)).reduce(0, (a, b) -> a + b);
        apitalflow.setSumosal(new BigDecimal(reduce.toString()));

        //查询所有销售总账
        List<EverSalerecord> salerecordList = salerecordMapper.selectList(null);
        BigDecimal reduce1 = salerecordList.stream().map(EverSalerecord::getMoney).reduce(new BigDecimal(0), ( a, b ) -> BigDecimal.valueOf(a.doubleValue() + b.doubleValue()));
        apitalflow.setSumbuy(reduce1);

        //查询所有的工资  (财务已经确认的)
        QueryWrapper<EverSalary> everSalaryQueryWrapper = new QueryWrapper<>();
        everSalaryQueryWrapper.eq("salstate","2");
        List<EverSalary> everSalaries = salaryMapper.selectList(everSalaryQueryWrapper);
        BigDecimal reduce2 = everSalaries.stream().map(EverSalary::getCommission).reduce(new BigDecimal(0), ( a, b ) -> BigDecimal.valueOf(a.doubleValue() + b.doubleValue()));
        Double reduce3 = everSalaries.stream().map(EverSalary::getSal).map(b -> Double.parseDouble(b)).reduce(0.0, ( a, b ) -> a + b);
//        总和
        BigDecimal bigDecimal = BigDecimal.valueOf(reduce3.doubleValue() + reduce2.doubleValue());
        apitalflow.setSums(bigDecimal);

        // -- 以下代码不应该有(应该在产生 订单或 销售的时候完成资金更新)
        //公司账号账户总余额  (定金+销售 - 已发工资)
        double v = reduce + reduce1.doubleValue() - bigDecimal.doubleValue();
        BigDecimal bigDecimal1 = new BigDecimal(v);
        apitalflow.setMoney(bigDecimal1);
        //修改公司总账  数据回填
        EverApitalflow af = new EverApitalflow();
        af.setAno(apitalflow.getAno());af.setMoney(apitalflow.getMoney());
        apitalflowMapper.updateById(af);
        return apitalflow;
    }

    /**
     * 周期性 查询公司流水
     * @param day
     * @return
     */
    @Override
    public List<EverOrderandsale> findEvery(Integer day ,Date starttime, Date endtime,Integer current, Integer size) {
        //首先按照 day判断的周期 前端固定默认为 1天[1]    7天（一周）[2]  30天（一月）[3]
//        int i = day == 1 ? 1 : day < 7 ? 2 : 3;
        //条件
        QueryWrapper<EverOrderandsale> qw = new QueryWrapper<>();
        qw.ge("date",starttime);
        qw.le("date",endtime);
        qw.orderByDesc("date");
        qw.groupBy("date");
        //分页
        Page<EverOrderandsale> page = new Page<>(current,size);
        IPage<EverOrderandsale> orderandsaleList = orderandsaleMapper.selectSalary(page,qw);
        List<EverOrderandsale> records = orderandsaleList.getRecords();
       if(records.size()>0){
        records.get(0).setSum((int)orderandsaleList.getTotal());}
        return records;
    }

    @Override
    public EverApitalflow glosal( EverApitalflow ap ) {
        int i = apitalflowMapper.updateById(ap);
        return apitalflowMapper.selectById(ap.getAno());
    }


}
