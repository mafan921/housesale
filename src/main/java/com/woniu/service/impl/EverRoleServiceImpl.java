package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.woniu.entity.EverEmp;
import com.woniu.entity.EverPermission;
import com.woniu.entity.EverRole;
import com.woniu.mapper.EverEmpMapper;
import com.woniu.mapper.EverRoleMapper;
import com.woniu.service.IEverRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Service
public class EverRoleServiceImpl extends ServiceImpl<EverRoleMapper, EverRole> implements IEverRoleService {
        @Autowired
        EverRoleMapper rmapper;
        @Autowired
        EverEmpMapper emapper;
    @Override
    public EverRole findEmpAllRoles(Long rid) {
        QueryWrapper<EverRole> qw=new QueryWrapper();
        qw.in("rid",rid);
        List<EverRole> empAllRoles = rmapper.selectList(qw);
        return empAllRoles.size()!=0?empAllRoles.get(0):null;
    }



    @Override
    public List<EverRole> findAllRoles() {

        return rmapper.selectList(null);
    }

    @Override
    public Long findRidByRname(String rname) {
        QueryWrapper<EverRole> qw=new QueryWrapper<>();
        qw.eq("rname",rname);
        List<EverRole> roles = rmapper.selectList(qw);

        return roles.size()!=0?roles.get(0).getRid():null;
    }
}
