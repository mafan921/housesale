package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverOrder;
import com.woniu.entity.EverSalerecord;
import com.woniu.mapper.EverOrderMapper;
import com.woniu.mapper.EverSalerecordMapper;
import com.woniu.service.IEverSalerecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Service
public class EverSalerecordServiceImpl extends ServiceImpl<EverSalerecordMapper, EverSalerecord> implements IEverSalerecordService {
    @Autowired
    private EverSalerecordMapper everSalerecordMapper;


    @Override
    public Map<String, Object> ManagerFindAllSL(EverSalerecord salerecord, Integer pageNo, Integer pageSize) {
        Map<String, Object> map = new HashMap<>();
        //参数一是当前页，参数二是每页个数
        QueryWrapper<EverOrder> queryWrapper = new QueryWrapper<>();
        if (salerecord.getEid() != null && !"".equals(salerecord.getEid())) {
            queryWrapper.eq("s.eid", salerecord.getEid());
        }

        if (salerecord.getBegin() != null && !"".equals(salerecord.getBegin())) {
            queryWrapper.gt("stime", salerecord.getBegin());
        }

        if (salerecord.getEnd() != null && !"".equals(salerecord.getEnd())) {
            queryWrapper.lt("stime", salerecord.getEnd());
        }

        Page<EverOrder> page = new Page<>(pageNo, pageSize);
        IPage<EverOrder> pageData = everSalerecordMapper.selectSLByManager(page, queryWrapper);
        System.out.println("总页数：" + pageData.getPages());
        System.out.println("总记录数：" + pageData.getTotal());

        List<EverOrder> list = pageData.getRecords();

        map.put("list", list);
        map.put("totalPage", pageData.getTotal());

        return map;

    }

    @Override
    public List<EverSalerecord> reportMoney(EverSalerecord salerecord) {
        //参数一是当前页，参数二是每页个数
        QueryWrapper<EverSalerecord> queryWrapper = new QueryWrapper<>();

        if (salerecord.getBegin() != null && !"".equals(salerecord.getBegin())) {
            queryWrapper.gt("stime", salerecord.getBegin());
        }

        if (salerecord.getEnd() != null && !"".equals(salerecord.getEnd())) {
            queryWrapper.lt("stime", salerecord.getEnd());
        }
        List<EverSalerecord> lists = everSalerecordMapper.selectList(queryWrapper);
        List<EverSalerecord> result = new ArrayList<>();
            for  ( int  i  =   0 ; i  <  lists.size()  -   1 ; i ++ )  {
                for  ( int  j  =  lists.size()  -   1 ; j  >  i; j -- )  {

                    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                    String s=sdf.format(lists.get(i).getStime());
                    String s1=sdf.format(lists.get(j).getStime());
                    if  (s1.equals(s))  {
                        lists.get(i).setMoney(lists.get(i).getMoney().add(lists.get(j).getMoney()));
                        lists.remove(j);
                    }
                }
            }
        lists.forEach(System.out::println);
        return lists;
    }
}
