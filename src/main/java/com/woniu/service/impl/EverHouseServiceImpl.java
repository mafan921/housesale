package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverEmp;
import com.woniu.entity.EverHouse;
import com.woniu.entity.EverSalary;
import com.woniu.mapper.EverHouseMapper;
import com.woniu.service.IEverHouseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Service
@Transactional
public class EverHouseServiceImpl implements IEverHouseService {
    @Autowired
    private EverHouseMapper everHouseMapper;

    @Override
    public List<EverHouse> findAll(Integer current, Integer size) {
        //分页
          Page<EverHouse> page=new Page<>(current,size);
          QueryWrapper<EverHouse> qw=new QueryWrapper<>();
          //查所有
          Page<EverHouse> pageList=everHouseMapper.selectPage(page,qw);
          page.getRecords().forEach(System.out::println);
          List<EverHouse> houseList=page.getRecords();
          return houseList;
    }

    /**
     * 模糊查询加分页
     * @param house
     * @param size
     * @param current
     * @return
     */
    @Override
    public List<EverHouse> selectByWrapper(EverHouse house,Integer size, Integer current) {
        IPage<EverHouse> page = new Page<>(current, size);
        QueryWrapper<EverHouse> wrapper = new QueryWrapper<>();
        //查询条件
        if (house.getStyle()!=null&&house.getStyle()!=""){
            wrapper.like("style", house.getStyle());
        }
        if (house.getDirection()!=null&&house.getDirection()!=""){
            wrapper.eq("direction", house.getDirection());
        }
        if (house.getArea()!=null&&house.getArea()!=""){
            wrapper.eq("area", house.getArea());
        }
        if (house.getMinprice()!=null&&!house.getMinprice().equals("")){
            wrapper.ge("price", house.getMinprice());
        }
        if (house.getMaxprice()!=null&&!house.getMaxprice().equals("")){
            wrapper.le("price", house.getMaxprice());
        }
        Object value = house.getValue();
        if (value instanceof ArrayList){
            List list= (List) house.getValue();
        }else if (value instanceof String){
            String str= (String) house.getValue();
            String[] split = str.split(",");
            if (split.length==3){
                wrapper.eq("bname1", split[0]);
                wrapper.eq("baddr1", split[1]);
                wrapper.eq("bdeveloper1", split[2]);
            }
        }
        //按条件查询
        List<EverHouse> sums=everHouseMapper.selectList(wrapper);
        IPage<EverHouse> everHousePage = everHouseMapper.select1(wrapper,page);
        //后台打印
        page.getRecords().forEach(System.out::println);
        //转换List，传总数
        List<EverHouse> houseList=new ArrayList<>();
        for (EverHouse h: everHousePage.getRecords()) {
            h.setSum(sums.size());
            houseList.add(h);
        }
        return houseList;
    }

    /**
     * 新增
     * @param everHouse
     */

    @Override
    public void save(EverHouse everHouse) {
        everHouse.setBstate(0L);
        everHouse.setIskill("1");
        everHouse.setSoftdel(1);
        if (everHouse.getStyle().equals("三室两厅一卫")||everHouse.getStyle().equals("三室一厅两卫")){
            everHouse.setArea("135.69");
        }else if (everHouse.getStyle().equals("三室一厅一卫")||everHouse.getStyle().equals("两室两厅两卫")){
            everHouse.setArea("125.69");
        }else if (everHouse.getStyle().equals("三室两厅两卫")){
            everHouse.setArea("145.69");
        }else if (everHouse.getStyle().equals("一室一厅一卫")){
            everHouse.setArea("69.69");
        }else if (everHouse.getStyle().equals("两室一厅一卫")){
            everHouse.setArea("89.69");
        }else if (everHouse.getStyle().equals("两室两厅一卫")){
            everHouse.setArea("109.69");
        }
//        everHouse.setHid(everHouse.getBname()+everHouse.getBaddr()+everHouse.getBdeveloper()+"0"+everHouse.getNum());
        String a=everHouse.getBname1()+"-"+everHouse.getBaddr1()+"-"+everHouse.getBdeveloper1()+"0"+everHouse.getNum();
        everHouse.setHunm(a);
        everHouseMapper.insert(everHouse);
    }

    /**
     * 0  未卖出
     * @param hids
     */
    @Override
    public void deleteByList(Long[] hids) {
        for (Long i : hids) {
            EverHouse house=new EverHouse();
            house.setHid(i);
            house.setSoftdel(0);
            everHouseMapper.updateById(house);
        }
    }

    /**
     * 修改
     * @param everHouse
     */
    @Override
    public void update(EverHouse everHouse) {
        everHouseMapper.updateById(everHouse);
    }
}
