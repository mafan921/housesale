package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.woniu.entity.EverPermission;
import com.woniu.mapper.EverPermissionMapper;
import com.woniu.service.IEverPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Service
public class EverPermissionServiceImpl extends ServiceImpl<EverPermissionMapper, EverPermission> implements IEverPermissionService {
        @Autowired
        EverPermissionMapper pMapper;
        @Override
        public List<EverPermission> findRoleAllPermissions(List<Long> pids) {
                QueryWrapper<EverPermission> qw=new QueryWrapper();
                qw.in("pid",pids);
                List<EverPermission> roleAllPermissions = pMapper.selectList(qw);
                return roleAllPermissions;
        }

        @Override
        public List<EverPermission> findAllPermissions() {

                List<EverPermission> permissions = pMapper.selectList(null);
                for (EverPermission permission : permissions) {
                        if(permission.getParentid()!=null&&permission.getParentid()!=0){
                                QueryWrapper<EverPermission> qw=new QueryWrapper();
                                qw.eq("pid",permission.getParentid());
                                List<EverPermission> p = pMapper.selectList(qw);
                                permission.setParentName(p.get(0).getPname());
                        }
                }

                return permissions;
        }

        @Override
        public List<EverPermission> findPermissionByPids(List<Long> pids) {

                List<EverPermission> permissions=new ArrayList<>();
                if (pids.size()!=0) {
                for (Long pid : pids) {
                        EverPermission permission = pMapper.selectById(pid);
                        permissions.add(permission);
                }
                }
                return permissions.size()!=0?permissions:null;
        }
}
