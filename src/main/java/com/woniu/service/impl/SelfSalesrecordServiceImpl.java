package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.entity.*;
import com.woniu.mapper.*;
import com.woniu.service.SelfSalesrecordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : mafan
 * @date : 2020-12-02 20:33
 **/
@Service
@Transactional
public class SelfSalesrecordServiceImpl extends ServiceImpl<EverSalerecordMapper, EverSalerecord> implements SelfSalesrecordService {

    @Resource
    private EverAppointmentMapper appointmentMapper;
    @Resource
    private EverSalerecordMapper salerecordMapper;
    @Resource
    private EverHouseMapper houseMapper;
    @Resource
    private EverOrderMapper orderMapper;
    @Resource
    private EverOrderandsaleMapper orderandsaleMapper;


    @Override
    public void updateBySaleid(EverSalerecord salerecord) {
        salerecordMapper.updateById(salerecord);
    }

    @Override
    public List<EverSalerecord> findAllRec() {
        QueryWrapper<EverSalerecord> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("stime");
        List<EverSalerecord> salerecordList = salerecordMapper.selectList(wrapper);
        salerecordList.forEach(System.out::println);
        return salerecordList;
    }
    @Override
    public void saveRecord(EverAppointment appointment) {
        EverAppointment o = appointmentMapper.selectById(appointment.getAid());
        //根据预约新建销售记录
        EverSalerecord salerecord = new EverSalerecord();
        salerecord.setEid(o.getEid());
        salerecord.setHid(o.getHid());
        salerecord.setUid(o.getUid());
        salerecord.setStime(new Date());
        //获取房屋总价
        EverHouse house = houseMapper.selectById(o.getHid());
        System.out.println(house);
        Double value= (house.getPrice().doubleValue()) *Double.parseDouble(house.getArea());
        System.out.println("==========================================="+value);
        //double转BigDecimal
        BigDecimal d = BigDecimal.valueOf(value);
        System.out.println("=========================================房屋价格"+d);
        salerecord.setMoney(d);
        //将数据存入数据库
        salerecordMapper.insert(salerecord);
        //删除预约表中的数据
        appointmentMapper.deleteById(appointment);
        //软删除房屋
        EverHouse h= new EverHouse();
        h.setHid(o.getHid());
        h.setSoftdel(0);
        houseMapper.updateById(house);
        //往ever_orderandsale存资金变化
        EverOrderandsale oas = new EverOrderandsale();
        oas.setDate(new Date());
        oas.setHid(o.getHid().intValue());
        //总价
        BigDecimal a= BigDecimal.valueOf(value);
        oas.setSale(a);
        orderandsaleMapper.insert(oas);


    }
    @Override
    public void saveByOrd(EverOrder order) {
        EverOrder o = orderMapper.selectById(order.getOrdid());
        //根据订单新建销售记录
        EverSalerecord salerecord = new EverSalerecord();
        salerecord.setEid(o.getEid());
        salerecord.setHid(o.getHid());
        salerecord.setUid(o.getUid());
        salerecord.setStime(new Date());
        //获取房屋总价
        EverHouse house = houseMapper.selectById(o.getHid());
        System.out.println(house);
        Double value= (house.getPrice().doubleValue()) *Double.parseDouble(house.getArea());
        System.out.println("==========================================="+value);
        //double转BigDecimal
        BigDecimal d = BigDecimal.valueOf(value);
        System.out.println("=========================================房屋价格"+d);
        salerecord.setMoney(d);
        //将数据存入数据库
        salerecordMapper.insert(salerecord);
        //删除订单表中的数据
        orderMapper.deleteById(order);
        //往ever_orderandsale存资金变化
        EverOrderandsale oas = new EverOrderandsale();
        oas.setDate(new Date());
        oas.setHid(o.getHid().intValue());
           //总价减去订金
        BigDecimal a= BigDecimal.valueOf(value-50000);
        oas.setSale(a);
        orderandsaleMapper.insert(oas);


    }

    @Override
    public void deleteBySaleid(Long saleid) {

    }

    @Override
    public Map<String, Object> findAllSalrecord(Integer current, Integer size) {
        Page<EverSalerecord> page = new Page<>(current, size);
        QueryWrapper<EverSalerecord> wrapper = new QueryWrapper<>();
        wrapper
//                .eq("eid", "1")
                .orderByDesc("stime");
        IPage<EverSalerecord> salerecords = salerecordMapper.find2UH(page,wrapper);
        List<EverSalerecord> salerecordList = salerecords.getRecords();
        long total = salerecords.getTotal();
        Map<String, Object> map=new HashMap<>();
        map.put("list", salerecordList);
        map.put("total", total);

        return map;
    }


}
