package com.woniu.service.impl;

import com.woniu.entity.EverOrderandsale;
import com.woniu.mapper.EverOrderandsaleMapper;
import com.woniu.service.IEverOrderandsaleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-12-03
 */
@Service
public class EverOrderandsaleServiceImpl extends ServiceImpl<EverOrderandsaleMapper, EverOrderandsale> implements IEverOrderandsaleService {

}
