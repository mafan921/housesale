package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverAppointment;
import com.woniu.entity.EverOrder;
import com.woniu.mapper.EverAppointmentMapper;
import com.woniu.service.IEverAppointmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Service
public class EverAppointmentServiceImpl extends ServiceImpl<EverAppointmentMapper, EverAppointment> implements IEverAppointmentService {
    @Autowired
    private EverAppointmentMapper appointmentMapper;


    @Override
    public Map<String, Object> ManagerSelectAllByPage(EverAppointment everAppointment, Integer pageNo, Integer pageSize) {
        QueryWrapper queryWrapper=new QueryWrapper();
        Map<String, Object> map=new HashMap<>();
        if (everAppointment.getEid() != null && !"".equals(everAppointment.getEid())) {
            queryWrapper.eq("e.eid", everAppointment.getEid());
        }

        if (everAppointment.getBegin()!=null&&!"".equals(everAppointment.getBegin())){
            queryWrapper.ge("begintime", everAppointment.getBegin());
            queryWrapper.le("endtime", everAppointment.getEnd());
        }

        Page<EverOrder> page = new Page<>(pageNo, pageSize);

        // SELECT COUNT(1) FROM user WHERE (birthday = ?)
        // select * from user WHERE (birthday = ?) LIMIT ?
        IPage<EverOrder> pageData = appointmentMapper.selectAppByManager(page, queryWrapper);
        System.out.println("总页数：" + pageData.getPages());
        System.out.println("总记录数：" + pageData.getTotal());

        List<EverOrder> list = pageData.getRecords();
        map.put("orderList", list);
        map.put("totalPage", pageData.getTotal());

        return map;


    }
}
