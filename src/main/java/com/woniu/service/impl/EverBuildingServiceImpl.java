package com.woniu.service.impl;

import com.woniu.entity.EverBuilding;
import com.woniu.mapper.EverBuildingMapper;
import com.woniu.service.IEverBuildingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Service
public class EverBuildingServiceImpl implements IEverBuildingService {

    @Autowired
    private EverBuildingMapper mapper;

    @Override
    public List<EverBuilding> findAll() {
        return mapper.selectList(null);
    }
}
