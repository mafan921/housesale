package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.woniu.entity.EverPermission;
import com.woniu.entity.EverRole;
import com.woniu.entity.EverRolepermission;
import com.woniu.mapper.EverRolepermissionMapper;
import com.woniu.service.IEverRolepermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
@Service
public class EverRolepermissionServiceImpl extends ServiceImpl<EverRolepermissionMapper, EverRolepermission> implements IEverRolepermissionService {
            @Autowired
            EverRolepermissionMapper rpMapper;
    @Override
    public List<Long> findRoleAllPids(EverRole empRole) {
        QueryWrapper<EverRolepermission> qw=new QueryWrapper();

            qw.eq("rid",empRole.getRid());
        List<EverRolepermission> rps = rpMapper.selectList(qw);
        List<Long> pids = new ArrayList<>();

        for (EverRolepermission rp:rps) {
            pids.add(rp.getPid());
        }

        return pids.size()!=0?pids:null;
    }
}
