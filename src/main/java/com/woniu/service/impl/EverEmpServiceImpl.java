package com.woniu.service.impl;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverEmp;
import com.woniu.entity.EverEmprole;
import com.woniu.entity.EverHouse;
import com.woniu.entity.EverSalary;
import com.woniu.mapper.EverEmpMapper;
import com.woniu.mapper.EverEmproleMapper;
import com.woniu.service.IEverEmpService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.unit.PhoneLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Service
@Transactional
public class EverEmpServiceImpl implements IEverEmpService {
    @Resource
    private EverEmpMapper empMapper;
    @Autowired
    private StringRedisTemplate st;
    @Autowired
    private EverEmproleMapper emproleMapper;

    @Override
    public List<EverEmp> findAll( Integer current, Integer size ) {
        //分页
        Page<EverEmp> page = new Page<>(current, size);
        QueryWrapper<EverEmp> qw = new QueryWrapper<>();
        //查所有
        Page<EverEmp> pageList = empMapper.selectPage(page, qw);
        page.getRecords().forEach(System.out::println);
        List<EverEmp> houseList = page.getRecords();
        return houseList;
    }

    @Override
    public Integer updateById( EverEmp emp ) {
        return empMapper.updateById(emp);
    }

    @Override
    public void save( EverEmp emp ) {
        emp.setSoftdel(0);
        emp.setEpwd("123456");
        emp.setRname("置业顾问");
        empMapper.insert(emp);
        List<EverEmp> everEmps = empMapper.selectList(null);
        Long eid = everEmps.get(everEmps.size() - 1).getEid();
        EverEmprole emprole=new EverEmprole();
        emprole.setRid(2L);
        emprole.setEid(eid);
        emproleMapper.insert(emprole);
    }

    @Override
    public void delByList( Long[] eids ) {
        for (long i : eids) {
            EverEmp emp = new EverEmp();
            emp.setEid(i);
            emp.setSoftdel(1);
            empMapper.updateById(emp);
        }
    }

    @Override
    public List<EverEmp> selectByWrapper( EverEmp emp, Integer size, Integer current ) {
        Page<EverEmp> page = new Page<>(current, size);
        QueryWrapper<EverEmp> wrapper = new QueryWrapper<>();
        if (emp.getEid() != null && !emp.getEid().equals("")) {
            wrapper.eq("eid", emp.getEid());
        }
        if (emp.getEname() != null && !emp.getEname().equals("")) {
            wrapper.like("ename", emp.getEname());
        }
        if (emp.getSex() != null && !emp.getSex().equals("")) {
            wrapper.eq("sex", emp.getSex());
        }
        List<EverEmp> sums = empMapper.selectList(wrapper);
        Page<EverEmp> empPage = empMapper.selectPage(page, wrapper);
        //后台打印
        page.getRecords().forEach(System.out::println);

        List<EverEmp> empList = new ArrayList<>();
        for (EverEmp e : empPage.getRecords()) {
            e.setSum(sums.size());
            empList.add(e);
        }
        return empList;
    }

    @Override
    public EverEmp findLoginByAccount( String eaccount ) {
        QueryWrapper<EverEmp> qw = new QueryWrapper<>();
        qw.eq("eaccount", eaccount);
        return empMapper.selectOne(qw);
    }


    @Override
    public List<EverEmp> findAllNoPage() {
        return empMapper.selectList(null);
    }


    /**
     * 手机号验证 --获取验证码（发给对应的手机号）
     *
     * @param phone
     * @return
     */
    @Override
    public Map<String, Object> phonecode( String phone, HttpSession session ) {
        Map<String, Object> map = new HashMap<>();
        //通过前端传手机号 与数据库里的对比
        QueryWrapper<EverEmp> qw = new QueryWrapper<>();
        qw.eq("phone", phone);
        List<EverEmp> everEmps = empMapper.selectList(qw);
        //手机号与员工一一绑定
        if (everEmps.size() > 0) {
            try {
                //随机生成一个随机 6 位数
                int temp = (int) ((Math.random() * 9 + 1) * 100000);
                //执行工具类发短信验证码
//                SendSmsResponse sendSms = PhoneLogin.sendPhoneMessage(phone, temp+"");
                System.out.println(temp);
//                已字符串形式
                st.opsForValue().set(phone, temp + "");
                map.put("msg", true);
                return map;
            } catch (Exception e) {
                map.put("msg", "短信发送失败");
                return map;
            }
        } else {
            map.put("msg", "号码不存在");
            return map;
        }
    }

    /**
     * 手机号码登录--前端输入验证码，与redis中的对比
     *
     * @param code
     * @param session
     * @return
     */
    @Override
    public Map<String, Object> phoneLogin( String phone, String code, HttpSession session ) {
        Map<String, Object> map = new HashMap<>();
        //获取redis中保存的验证码    && st.opsForValue().get(phone).equals(code)
        if (code != null ) {
            map.put("msg", true);
            return map;
        } else {
            map.put("msg", false);
            return map;
        }
    }

    @Override
    public Map<String, Object> findMyself(HttpSession session, Integer eid) {
        EverEmp everEmp = empMapper.selectById(1);
        Map<String, Object> map=new HashMap<>();
        map.put("list", everEmp);

        return map;
    }

    @Override
    public Map<String, Object> updatePwd(EverEmp emp) {
        System.out.println("///////////////////"+emp);

        if (emp.getEpwd().equals(emp.getOldpwd())){
            EverEmp emp1 = new EverEmp();
            emp1.setEpwd(emp.getNewpwd());
            empMapper.updateById(emp1);
        }else {

        }
        return null;
    }
}