package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverLog;
import com.woniu.entity.EverSalary;
import com.woniu.mapper.EverLogMapper;
import com.woniu.service.IEverLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Service
@Transactional
public class EverLogServiceImpl extends ServiceImpl<EverLogMapper, EverLog> implements IEverLogService {
    @Resource
    private EverLogMapper logMapper;

    /**
     * 查询所有日志
     * @param starttime
     * @param endtime
     * @param current
     * @param size
     * @param ename
     * @param yl
     * @return
     */
    @Override
    public Page<EverLog> findAll( Date starttime,
                                  Date endtime,
                                  Integer current,
                                  Integer size,
                                  String ename,
                                  String yl) {
        Page<EverLog> page = new Page<>(current,size);
        QueryWrapper<EverLog> wp = new QueryWrapper<>();
        if(starttime != null && !starttime.equals("") ){
            wp.gt("date",starttime);
        }
        if(endtime != null && !endtime.equals("")) {
            wp.lt("date",endtime);
        }
        if(ename != null && !ename.equals("")){
            wp.like("ename",ename);
        }
        if(yl != null && !yl.equals("") && !yl.equals("all")) {
            wp.eq("yl",yl);
        }
        wp.orderByDesc("date");
        return logMapper.selectPage(page,wp);
    }

    /**
     * 查询所有的类型
     * @return
     */
    @Override
    public List<String> findStyle() {
        QueryWrapper<EverLog> wp = new QueryWrapper<>();
        wp.groupBy("yl");
        List<EverLog> everLogs = logMapper.selectList(wp);
        List<String> strings = new ArrayList<>();
        for (int i = 0; i < everLogs.size(); i++) {
            strings.add(everLogs.get(i).getYl());
        }
        return strings;
    }
}
