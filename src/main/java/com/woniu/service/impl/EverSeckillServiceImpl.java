package com.woniu.service.impl;

import com.woniu.entity.EverSeckill;
import com.woniu.mapper.EverSeckillMapper;
import com.woniu.service.IEverSeckillService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Service
public class EverSeckillServiceImpl extends ServiceImpl<EverSeckillMapper, EverSeckill> implements IEverSeckillService {

}
