package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverEmp;
import com.woniu.entity.EverOrder;
import com.woniu.mapper.EverEmpMapper;
import com.woniu.mapper.EverOrderMapper;
import com.woniu.service.IEverOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Service
public class EverOrderServiceImpl extends ServiceImpl<EverOrderMapper, EverOrder> implements IEverOrderService {
    @Resource
    private EverOrderMapper everOrderMapper;
    @Resource
    private EverEmpMapper everEmpMapper;


    @Override
    public Map<String, Object> findAllAndSearch(EverOrder order, Integer pageNo, Integer pageSize) {
        Map<String, Object> map=new HashMap<>();
        //参数一是当前页，参数二是每页个数
        QueryWrapper<EverOrder> queryWrapper = new QueryWrapper<>();
        if (order.getEid()!=null&&!"".equals(order.getEid())){
            queryWrapper.eq("o.eid",order.getEid());
        }

        if (order.getBegin()!=null&&!"".equals(order.getBegin())){
            queryWrapper.gt("datetime", order.getBegin());
        }
        if (order.getEnd()!=null&&!"".equals(order.getEnd())){
            queryWrapper.lt("datetime", order.getEnd());
        }

        Page<EverOrder> page = new Page<>(pageNo, pageSize);

        // SELECT COUNT(1) FROM user WHERE (birthday = ?)
        // select * from user WHERE (birthday = ?) LIMIT ?
        IPage<EverOrder> pageData = everOrderMapper.selectOrderByManager(page, queryWrapper);
        System.out.println("总页数：" + pageData.getPages());
        System.out.println("总记录数：" + pageData.getTotal());
        long total = pageData.getTotal();

        List<EverOrder> list = pageData.getRecords();
        map.put("orderList", list);
        map.put("totalPage", pageData.getTotal());

        return map;

    }
}
