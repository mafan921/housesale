package com.woniu.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverEmp;
import com.woniu.entity.EverSalary;
import com.baomidou.mybatisplus.extension.service.IService;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface IEverSalaryService extends IService<EverSalary> {

    List<EverSalary> findAll(String ename,
                             Integer current,
                             Integer size,
                             Date Starttime,
                             Date endtime,
                             String salstate
    );

    EverSalary findOne(Integer sid);

    Integer updateOne(EverSalary salary);

    Integer updateMany(Long[] sids);

    void genSalary() throws ParseException;

    Integer changeSalary(EverEmp emp);

    Page<EverSalary> findByEid(Integer eid, Integer size, Integer current);

    Integer returnSalary( Long sid );

    List<EverSalary> getsalary(String ename,
                               Date starttime,
                               Date endtime,
                               String salstate);
}
