package com.woniu.service;

import com.woniu.entity.EverBuilding;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.entity.EverEmp;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface IEverBuildingService{
    List<EverBuilding> findAll();
}
