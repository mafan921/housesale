package com.woniu.service;

import com.woniu.entity.EverOrderandsale;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-12-03
 */
public interface IEverOrderandsaleService extends IService<EverOrderandsale> {

}
