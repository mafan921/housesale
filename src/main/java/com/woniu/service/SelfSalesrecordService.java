package com.woniu.service;

import com.woniu.entity.EverAppointment;
import com.woniu.entity.EverOrder;
import com.woniu.entity.EverSalerecord;

import java.util.List;
import java.util.Map;

/**
 * @author : mafan
 * @date : 2020-12-02 20:33
 **/
public interface SelfSalesrecordService {
    void updateBySaleid(EverSalerecord salerecord);

    List<EverSalerecord> findAllRec();

    void saveByOrd(EverOrder order);

    void deleteBySaleid(Long saleid);

    Map<String, Object> findAllSalrecord(Integer current, Integer size);

    /**
     *
     * @param appointment
     */
    void saveRecord(EverAppointment appointment);
}
