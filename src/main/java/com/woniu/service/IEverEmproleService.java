package com.woniu.service;

import com.woniu.entity.EverEmp;
import com.woniu.entity.EverEmprole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-12-04
 */
public interface IEverEmproleService extends IService<EverEmprole> {
             Long findEmpAllRid(EverEmp emp);
             Long findEmpAllRidByAccount(EverEmp emp);



    void updateEmpRole(EverEmprole emprole);

    Long findEridByEid(Long eid);

    boolean save(EverEmprole emprole);
}
