package com.woniu.service;

import com.woniu.entity.EverUser;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.catalina.User;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface IEverUserService extends IService<EverUser> {

    void insertES() throws IOException, Exception;

    Integer updateOne(EverUser user) throws IOException, InterruptedException, Exception;

    Map<String, Object> findOne(EverUser user) throws IOException, Exception;

    String addUser(EverUser user) throws InterruptedException;

    String deleteById(Long uid) throws IOException, Exception;

    List<EverUser> findByExample(EverUser user);

    List<EverUser> findAllInEs();

    Map<String, Object> serachPageHighLight(EverUser user, Integer pageNo, Integer pageSize) throws IOException, Exception;

    void insertList(List<EverUser> list);

    List<EverUser> searchAll();
}
