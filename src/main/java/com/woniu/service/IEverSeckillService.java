package com.woniu.service;

import com.woniu.entity.EverSeckill;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface IEverSeckillService extends IService<EverSeckill> {

}
