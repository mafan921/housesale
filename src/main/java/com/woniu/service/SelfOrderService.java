package com.woniu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.entity.EverAppointment;
import com.woniu.entity.EverOrder;

import java.util.List;
import java.util.Map;

/**
 * @author : mafan
 * @date : 2020-12-01 21:09
 **/
public interface SelfOrderService  extends IService<EverOrder> {
    List<EverOrder> findtest(Long eid);

    List<EverOrder> findAllOrder();

    void saveOrder(EverOrder everOrder);

    void deleteByOrdid(Long ordid);

    void updateByOrdid(EverOrder order);

    void saveByAppt(EverAppointment appointment);

    Map<String, Object> findAllorder(Integer current, Integer size);


//    void findtest();
}
