package com.woniu.service;

import com.woniu.entity.EverPermission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface IEverPermissionService extends IService<EverPermission> {

    List<EverPermission> findRoleAllPermissions(List<Long> pids);

    List<EverPermission> findAllPermissions();

    List<EverPermission> findPermissionByPids(List<Long> pids);
}
