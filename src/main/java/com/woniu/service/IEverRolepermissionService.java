package com.woniu.service;

import com.woniu.entity.EverPermission;
import com.woniu.entity.EverRole;
import com.woniu.entity.EverRolepermission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
public interface IEverRolepermissionService extends IService<EverRolepermission> {
     List<Long> findRoleAllPids(EverRole empRole);
}
