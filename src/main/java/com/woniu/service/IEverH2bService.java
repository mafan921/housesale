package com.woniu.service;

import com.woniu.entity.EverH2b;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
public interface IEverH2bService extends IService<EverH2b> {

}
