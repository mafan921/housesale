package com.woniu.service;

import com.woniu.entity.EverEmp;
import com.woniu.entity.EverPermission;
import com.woniu.entity.EverRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface IEverRoleService extends IService<EverRole> {
    EverRole findEmpAllRoles(Long rid);

    List<EverRole> findAllRoles();
    Long findRidByRname(String rname);

}
