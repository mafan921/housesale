package com.woniu.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.entity.EverAppointment;

import java.util.Map;

/**
 * @author : mafan
 * @date : 2020-12-01 21:01
 **/
public interface SelfApptService extends IService<EverAppointment> {
    Page<EverAppointment> findByEid(Long eid, Integer current, Integer size);

    void saveOfupdate(EverAppointment appointment);

    void deleteAppt(Long aid);

    Map<String, Object> findAllAppt(Integer current, Integer size);
}
