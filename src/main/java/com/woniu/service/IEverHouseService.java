package com.woniu.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverEmp;
import com.woniu.entity.EverHouse;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface IEverHouseService{
    List<EverHouse> findAll(Integer current,
                            Integer size);

    List<EverHouse> selectByWrapper(EverHouse house,Integer size, Integer current);

    void save(EverHouse everHouse);

    void deleteByList(Long[] hids);

    void update(EverHouse everHouse);

}
