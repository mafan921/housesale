package com.woniu.service;

import com.woniu.entity.EverSalerecord;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface IEverSalerecordService extends IService<EverSalerecord> {

    Map<String, Object> ManagerFindAllSL(EverSalerecord salerecord, Integer pageNo, Integer pageSize);

    List<EverSalerecord> reportMoney(EverSalerecord salerecord);
}
