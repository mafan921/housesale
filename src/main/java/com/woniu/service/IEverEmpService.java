package com.woniu.service;

import com.woniu.entity.EverEmp;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.entity.EverHouse;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface IEverEmpService {
    List<EverEmp> findAll(Integer current, Integer size);

    Integer updateById(EverEmp emp);

    void save(EverEmp emp);

    void delByList(Long[] eids);

    List<EverEmp> selectByWrapper(EverEmp emp, Integer size, Integer current);

    EverEmp findLoginByAccount(String eaccount);

    List<EverEmp> findAllNoPage();

    Map<String, Object> phonecode( String phone,HttpSession session);

    Map<String, Object> phoneLogin(String phone, String code, HttpSession session );

    Map<String, Object> findMyself(HttpSession session, Integer eid);

    Map<String, Object> updatePwd(EverEmp emp);
}
