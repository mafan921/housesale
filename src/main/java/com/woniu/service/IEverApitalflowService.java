package com.woniu.service;

import com.woniu.entity.EverApitalflow;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.entity.EverOrderandsale;
import com.woniu.entity.EverSalerecord;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
public interface IEverApitalflowService extends IService<EverApitalflow> {

    EverApitalflow find();

    List<EverOrderandsale> findEvery( Integer day, Date starttime, Date endtime, Integer current, Integer size);


    EverApitalflow glosal( EverApitalflow ap );
}
