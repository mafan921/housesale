package com.woniu.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.entity.EverLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
public interface IEverLogService extends IService<EverLog> {

    Page<EverLog> findAll( Date starttime,
                           Date endtime,
                           Integer current,
                           Integer size,
                           String ename,
                           String yl);

    List<String> findStyle();
}
