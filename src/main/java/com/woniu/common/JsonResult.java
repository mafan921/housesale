package com.woniu.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author : mafan
 * @date : 2020-12-02 21:44
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JsonResult implements Serializable {
    /**
     * @ApiModelProperty("响应码")
     */
    private Integer code;
    /**
     * ApiModelProperty("响应消息")
     */
    private String msg;
    /**
     * @ApiModelProperty("响应数据")
     */
    private Object data;

    public JsonResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static JsonResult ok() {
        JsonResult result = new JsonResult();
        result.setCode(200);
        result.setMsg("OK");
        return result;
    }
    public static JsonResult ok(Object data) {
        JsonResult result = new JsonResult();
        result.setCode(200);
        result.setMsg("OK");
        result.setData(data);
        return result;
    }
    public static JsonResult fail(String msg) {
        JsonResult result = new JsonResult();
        result.setCode(500);
        result.setMsg(msg);
        return result;
    }
}
