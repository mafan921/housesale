package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverSalerecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 预约id
     */
      @TableId(value = "saleid", type = IdType.ASSIGN_ID)
    private Long saleid;

    /**
     * 客户id
     */
    private Long uid;

    /**
     * 置业顾问id
     */
    private Long eid;

    /**
     * 房源id
     */
    private Long hid;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date stime;

    private BigDecimal money;
    //数据库中没有
    @TableField(exist = false)
    private String uname;

    @TableField(exist = false)
    private String hunm;

    @TableField(exist = false)
    private String bname;

    //数据库中没有
    @TableField(exist = false)
    private String ename;
    @TableField(exist = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date begin;
    @TableField(exist = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date end;

}
