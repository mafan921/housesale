package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverUserrole implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "urid", type = IdType.AUTO)
    private Integer urid;

    /**
     * 角色
     */
    private Integer uid;

    /**
     * 用户
     */
    private Integer rid;


}
