package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverRolepermission implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "rpid", type = IdType.AUTO)
    private Long rpid;

    /**
     * 权限
     */
    private Long pid;

    /**
     * 角色
     */
    private Long rid;


}
