package com.woniu.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-12-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverOrderandsale implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "osid", type = IdType.AUTO)
    private Integer osid;

    /**
     * 房间id
     */
    private Integer hid;

    /**
     * 产生的日期
     */
    private Date date;

    /**
     * 定金金额
     */
    private BigDecimal order;

    /**
     * 销售金额
     */
    private BigDecimal sale;

    /**
     * 订单状态
     */
    private String state;

    @TableField(exist = false)
    private BigDecimal summoney;
    @TableField(exist = false)
    private Integer sum;
}
