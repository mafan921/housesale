package com.woniu.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverSalary implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 工资id、
     */

      @TableId(value = "sid", type = IdType.ASSIGN_ID)
    private Long sid;

    /**
     * 基础工资
     */
    private String sal;

    /**
     * 员工id
     */
    private Long eid;

    /**
     * 提成
     */
    private BigDecimal commission;

    /**
     * 时间
     */
    private Date saltime;

    /**
     * 工资状态
     */
    private String salstate;
    @TableField(exist = false)
    private EverEmp emp;

    @TableField(exist = false)
    private Integer sum;  //用于记录当前查询的总条数

}
