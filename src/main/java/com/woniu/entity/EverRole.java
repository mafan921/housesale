package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverRole implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "rid", type = IdType.ASSIGN_ID)
    private Long rid;

    /**
     * 角色名
     */
    private String rname;

    /**
     * 软删除0/1
     */
    private Integer softdel;
    @TableField(exist = false)
    private  List<String> pnames;
}
