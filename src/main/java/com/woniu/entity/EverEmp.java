package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverEmp implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 员工id
     */
      @TableId(value = "eid", type = IdType.ASSIGN_ID)
    private Long eid;

      @TableField(exist = false)
      private long sid;
    /**
     * 总工资
     */
    @TableField(exist = false)  //数据库不存在此列
    private Double sumsalary;

    /**
     * 基础工资
     */
    private String sal;
    /**
     * 员工名
     */
    private String ename;

    /**
     * 账号
     */
    private String eaccount;

    /**
     * 密码
     */
    private String epwd;

    /**
     * 手机
     */
    private String phone;

    /**
     * 性别
     */
    private String sex;

    /**
     * 头像
     */
    private String photo;

    /**
     * 银行卡号
     */
    private String bankcard;



    /**
     * 软删除0/1
     */
    private Integer softdel;

    @TableField(exist = false)
    private Integer sum;

    @TableField(exist = false)
    private String oldpwd;
    @TableField(exist = false)
    private String newpwd;

    @TableField(exist = false)
    private String rname;

}
