package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-12-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverEmprole implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "erid", type = IdType.ASSIGN_ID)
    private Long erid;

    private Long eid;

    private Long rid;


}
