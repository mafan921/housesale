package com.woniu.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverApitalflow implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 公司id
     */
      @TableId(value = "ano", type = IdType.ASSIGN_ID)
    private Long ano;

    /**
     * 公司账户
     */
    private Long account;

    /**
     * 余额
     */

    private BigDecimal money;

    private BigDecimal glosal;

    //所有订金的总和
    @TableField(exist = false)
    private BigDecimal sumosal;

    //所有的销售综合
    @TableField(exist = false)
    private BigDecimal sumbuy;

    //所有的工资综合
    @TableField(exist = false)
    private BigDecimal sums;

}
