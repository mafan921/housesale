package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日志
     */
      @TableId(value = "lid", type = IdType.ASSIGN_ID)
    private Long lid;

    /**
     * 员工
     */
    private Long eid;
    private String ename;

    /**
     * 操作时间
     */
    private Date date;

    /**
     * 操作记录
     */
    private String op;
    private String ip;
    private String yl;

    @TableField(exist = false)
    private Integer sum;

}
