package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverAppointment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 预约id
     */
      @TableId(value = "aid", type = IdType.ASSIGN_ID)
    private Long aid;

    /**
     * 客户id
     */
    private Long uid;

    /**
     * 预约开始时间
     */
//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date begintime;

    /**
     * 预约结束时间
     */
//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endtime;

    /**
     * 置业顾问
     */
    private Long eid;

    /**
     * 房源id
     */
    private Long hid;

    /**
     * 预约状态
     */
    private String appstate;

    private String log;

    @TableField(exist = false)
    private String uname;
    @TableField(exist = false)
    private String ename;
    @TableField(exist = false)
    private String hunm;

    @TableField(exist = false)
    private String phone;

    @TableField(exist = false)
    private Date begin;
    @TableField(exist = false)
    private Date end;
    @TableField(exist = false)
    private Integer sum;  //用于记录当前查询的总条数


}
