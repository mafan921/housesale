package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;

import java.time.LocalDateTime;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-12-03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class EverUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
      @TableId(value = "uid", type = IdType.ASSIGN_ID)
    private Long uid;

    /**
     * 用户名
     */
    private String uname;

    /**
     * 账号
     */
    private String uaccount;

    /**
     * 密码
     */
    private String upwd;

    /**
     * 手机
     */
    private String phone;

    /**
     * 性别
     */
    private String sex;

    /**
     * 身份证号
     */
    private String idcard;

    /**
     * 软删除0/1
     */
    private Integer softdel;

    /**
     * 操作时间
     */
    @TableField(value = "creatTime",fill = FieldFill.INSERT)
    private LocalDateTime creatTime;

    /**
     * 地址
     */
    private String address;

    public EverUser(String uname, String uaccount, String upwd, String phone, String sex, String idcard, Integer softdel, String address) {
        this.uname = uname;
        this.uaccount = uaccount;
        this.upwd = upwd;
        this.phone = phone;
        this.sex = sex;
        this.idcard = idcard;
        this.softdel = softdel;
        this.address = address;
    }
}
