package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverSeckill implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 秒杀id
     */
      @TableId(value = "killid", type = IdType.ASSIGN_ID)
    private Integer killid;

    /**
     * 活动内容
     */
    private String content;

    /**
     * 开始时间
     */
    private Date begintime;

    /**
     * 秒杀状态
     */
    private String killstate;

    /**
     * 预定人数
     */
    private Integer knum;

    /**
     * 持续时间
     */
    private Integer contime;

    /**
     * 房源id
     */
    private Long hid;


}
