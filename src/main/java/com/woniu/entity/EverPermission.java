package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverPermission implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 权限id
     */
      @TableId(value = "pid", type = IdType.ASSIGN_ID)
    private Long pid;

    /**
     * 权限名
     */
    private String pname;

    /**
     * 功能链接
     */
    private String url;

    /**
     * 父权限
     */

    private Long parentid;

    /**
     * 软删除0/1
     */
    private Integer softdel;
    @TableField(exist = false)
    private String parentName;
}
