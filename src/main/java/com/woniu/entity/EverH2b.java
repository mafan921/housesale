package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverH2b implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "hbid", type = IdType.AUTO)
    private Integer hbid;

    /**
     * 房间
     */
    private Integer big;

    /**
     * build
     */
    private Integer hid;


}
