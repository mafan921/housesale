package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EverBuilding implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 楼盘id
     */
      @TableId(value = "big", type = IdType.ASSIGN_ID)
    private Long big;

    /**
     * 楼盘名
     */
    private String bname;

    /**
     * 楼盘地址
     */
    private String baddr;

    /**
     * 开发商
     */
    private String bdeveloper;


}
