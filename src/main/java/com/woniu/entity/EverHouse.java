package com.woniu.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author lkl
 * @since 2020-11-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class EverHouse implements Serializable {

    private static final long serialVersionUID = 1L;

    public EverHouse(String hunm,String style,String direction,BigDecimal minprice,BigDecimal maxprice,Object value){
        System.out.println("EverHouse.EverHouse");
        this.hunm=hunm;
        this.style=style;
        this.direction=direction;
        this.minprice=minprice;
        this.maxprice=maxprice;
        this.value=value;
    }

    /**
     * 房源id
     */
      @TableId(value = "hid", type = IdType.ASSIGN_ID)
    private Long hid;

    /**
     * 房间号
     */
    private String hunm;

    /**
     * 户型
     */
    private String style;

    /**
     * 朝向
     */
    private String direction;

    /**
     * 面积
     */
    private String area;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 软删除0/1
     */
    private Integer softdel;

    /**
     * 秒杀
     */
    private String iskill;

    /**
     * 楼号
     */
    private String bname1;

    /**
     * 单元号
     */
    private String baddr1;

    /**
     * 层号
     */
    private String bdeveloper1;

    /**
     * 户号
     */
    private String num;

    /**
     * 楼盘表
     */
    private Long bid;

    /**
     * 房源状态
     */
    private Long bstate;

    @TableField(exist = false)
    private Integer sum;

    @TableField(exist = false)
    private String name;

    @TableField(exist = false)
    private String addr;

    @TableField(exist = false)
    private String developer;

    @TableField(exist = false)
    private BigDecimal minprice;

    @TableField(exist = false)
    private BigDecimal maxprice;

    @TableField(exist = false)
    private Object value;



}
