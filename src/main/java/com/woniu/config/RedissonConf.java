package com.woniu.config;

import org.redisson.Redisson;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//宏锁 配置
//@Configuration
public class RedissonConf {
    @Bean
    public Redisson redisson(){
        Config config = new Config();
        config.useSingleServer().setAddress("redis://172.16.2.122:8001").setDatabase(0);
        config.useSingleServer().setPassword("1234");
        return (Redisson)Redisson.create(config);
    }
}
