package com.woniu.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ES配置文件
 * @author Nanf
 * @time 2020/11/29 19:39:15
 **/
//@Configuration
public class ElasticSearchClientConfig {
//    @Bean
    public RestHighLevelClient restHighLevelClient(){
        RestHighLevelClient restHighLevelClient=new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("172.16.2.49",9200,"http")
                ));
        return restHighLevelClient;
    }
}
